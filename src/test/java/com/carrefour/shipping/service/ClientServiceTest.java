package com.carrefour.shipping.service;


import com.carrefour.shipping.model.Client;
import com.carrefour.shipping.repository.ClientRepository;
import com.carrefour.shipping.service.dto.ClientDTO;
import com.carrefour.shipping.service.impl.ClientServiceImpl;
import com.carrefour.shipping.service.mapper.ClientMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ClientMapper clientMapper;

    @InjectMocks
    private ClientServiceImpl clientService;


    @Test
    public void testAddNewClient() {
        Client client = new Client();
        client.setId(1L);
        Mockito.when(clientRepository.save(client)).thenReturn(client);

        Optional<ClientDTO> result = clientService.addNewClient(client);

        assertTrue(result.isPresent());
    }

    @Test
    public void testUpdateClient() {
        Client client = new Client();
        client.setId(1L);
        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));

        Optional<ClientDTO> result = clientService.updateClient(client);

        assertTrue(result.isPresent());
    }

    @Test
    public void testFindAllClients() {
        List<Client> clients = new ArrayList<>();
        clients.add(new Client());
        clients.add(new Client());
        when(clientRepository.findAll()).thenReturn(clients);

        Optional<List<ClientDTO>> result = clientService.findAllClients();

        assertTrue(result.isPresent());
    }

    @Test
    public void testGetClientById() {
        Client client = new Client();
        client.setId(1L);
        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));

        Optional<ClientDTO> result = clientService.getClientById(1L);

        assertTrue(result.isPresent());
    }

    @Test
    public void testDeleteClient() {
        Long id = 1L;
        when(clientRepository.findById(id)).thenReturn(Optional.of(new Client()));

        Optional<Boolean> result = clientService.deleteClient(id);

        assertTrue(result.isPresent());
        assertTrue(result.get());
        verify(clientRepository, times(1)).delete(any());
    }
}
