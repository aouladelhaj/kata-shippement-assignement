package com.carrefour.shipping.service;

import com.carrefour.shipping.model.Shipping;
import com.carrefour.shipping.repository.ShippingRepository;
import com.carrefour.shipping.service.dto.ShippingDto;
import com.carrefour.shipping.service.impl.ShippingServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ShippingServiceTest {

    @Mock
    private ShippingRepository shippingRepository;

    @InjectMocks
    private ShippingServiceImpl shippingService;


    @Test
    public void testCreateShipping() {
        Shipping delivery = new Shipping();
        delivery.setId(1L);
        when(shippingRepository.save(delivery)).thenReturn(delivery);

        Optional<ShippingDto> result = shippingService.createShipping(delivery);

        assertTrue(result.isPresent());
    }

    @Test
    public void testUpdateShipping() {
        Shipping delivery = new Shipping();
        delivery.setId(1L);
        when(shippingRepository.findById(1L)).thenReturn(Optional.of(delivery));

        Optional<ShippingDto> result = shippingService.updateShipping(delivery);

        assertTrue(result.isPresent());
    }

    @Test
    public void testGetAllShippings() {
        List<Shipping> deliveries = new ArrayList<>();
        deliveries.add(new Shipping());
        deliveries.add(new Shipping());
        when(shippingRepository.findAll()).thenReturn(deliveries);

        Optional<List<ShippingDto>> result = shippingService.getAllShippings();

        assertTrue(result.isPresent());
        assertEquals(2, result.get().size());
    }

    @Test
    public void testFindShippingById() {
        Shipping delivery = new Shipping();
        delivery.setId(1L);
        when(shippingRepository.findById(1L)).thenReturn(Optional.of(delivery));

        Optional<ShippingDto> result = shippingService.findShippingById(1L);

        assertTrue(result.isPresent());
    }

    @Test
    public void testDeleteShipping() {
        Long id = 1L;
        when(shippingRepository.findById(id)).thenReturn(Optional.of(new Shipping()));

        Optional<Boolean> result = shippingService.deleteShipping(id);

        assertTrue(result.isPresent());
        assertTrue(result.get());
        verify(shippingRepository, times(1)).delete(any());
    }
}
