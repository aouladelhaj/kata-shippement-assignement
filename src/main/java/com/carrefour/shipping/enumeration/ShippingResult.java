package com.carrefour.shipping.enumeration;


/**
 * status of delivery process
 */
public enum ShippingResult {
    SCHEDULED,
    IN_PROGRESS,
    DELIVERED,
    CANCELLED,
}
