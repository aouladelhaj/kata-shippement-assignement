package com.carrefour.shipping.enumeration;

/**
 * carrefour delivery mode  enumeration.
 */
public enum ShippingMode {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP,

}
