package com.carrefour.shipping.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public class ShippingRespone<T>{
    private HttpStatus status;
    private String message;
    private T data;
}
