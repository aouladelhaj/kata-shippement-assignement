package com.carrefour.shipping.service;

import com.carrefour.shipping.model.Client;
import com.carrefour.shipping.service.dto.ClientDTO;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Optional<ClientDTO> addNewClient(Client client);
    Optional<ClientDTO> updateClient(Client client);
    Optional<List<ClientDTO>> findAllClients();
    Optional<ClientDTO> getClientById(Long id);
    Optional<Boolean> deleteClient(Long id);
}
