package com.carrefour.shipping.service.dto;

import com.carrefour.shipping.enumeration.ShippingMode;
import com.carrefour.shipping.enumeration.ShippingResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShippingDto {

    private Long id;
    private ShippingMode mode;
    private LocalTime deliveryStartTime;
    private LocalTime deliveryEndTime;
    private LocalDate deliveryDate;
    private ShippingResult status;
    private String deliveryAddress;
    private Long clientId;

}


