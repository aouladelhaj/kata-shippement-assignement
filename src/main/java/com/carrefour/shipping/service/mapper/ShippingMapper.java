package com.carrefour.shipping.service.mapper;

import com.carrefour.shipping.model.Shipping;
import com.carrefour.shipping.service.dto.ShippingDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ShippingMapper {
    ShippingMapper INSTANCE = Mappers.getMapper(ShippingMapper.class);

    ShippingDto toShippingDto(Shipping delivery);

    List<ShippingDto> toListShippingDto(List<Shipping> delivery);

}
