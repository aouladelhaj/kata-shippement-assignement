package com.carrefour.shipping.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.carrefour.shipping.model.Client;
import com.carrefour.shipping.service.dto.ClientDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClientMapper {
    ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

    ClientDTO clientToClientDTO(Client client);

    List<ClientDTO> clientsToClientDTO(List<Client> client);
}
