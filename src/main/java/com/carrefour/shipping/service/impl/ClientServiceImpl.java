package com.carrefour.shipping.service.impl;


import com.carrefour.shipping.model.Client;
import com.carrefour.shipping.repository.ClientRepository;
import com.carrefour.shipping.service.ClientService;
import com.carrefour.shipping.service.dto.ClientDTO;
import com.carrefour.shipping.service.mapper.ClientMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;


    @Transactional
    @Override
    public Optional<ClientDTO> addNewClient(Client client) {
        log.debug("starting Request to save Client : {}", client);
        return Optional.of(ClientMapper.INSTANCE.clientToClientDTO(clientRepository.save(client)));
    }


    @Transactional
    @Override
    public Optional<ClientDTO> updateClient(Client client) {
        log.debug("starting Request to update Client : {}", client);
        if (client.getId() != null) {
            var currentClient = clientRepository.findById(client.getId());
            return Optional.of(ClientMapper.INSTANCE.clientToClientDTO(currentClient.get()));
        }
        return Optional.of(ClientMapper.INSTANCE.clientToClientDTO(client));
    }


    @Transactional(readOnly = true)
    @Override
    public Optional<List<ClientDTO>> findAllClients() {
        log.debug(" starting Request to get all Clients");
        return Optional.of(ClientMapper.INSTANCE.clientsToClientDTO(clientRepository.findAll()));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ClientDTO> getClientById(Long id) {
        log.debug("starting Request to get Client : {}", id);
        return Optional.of(ClientMapper.INSTANCE.clientToClientDTO(clientRepository.findById(id).get()));
    }

    @Transactional
    @Override
    public Optional<Boolean> deleteClient(Long id) {
        log.debug(" starting Request to delete Client : {}", id);
        var entity = clientRepository.findById(id);
        if (entity != null) {
            clientRepository.delete(entity.get());
           return Optional.of(Boolean.TRUE);
        }
        return  Optional.of(Boolean.FALSE);
    }
}
