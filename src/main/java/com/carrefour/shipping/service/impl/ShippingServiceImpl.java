package com.carrefour.shipping.service.impl;

import com.carrefour.shipping.model.Shipping;
import com.carrefour.shipping.repository.ShippingRepository;
import com.carrefour.shipping.service.ShippingService;
import com.carrefour.shipping.service.dto.ShippingDto;
import com.carrefour.shipping.service.mapper.ShippingMapper;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class ShippingServiceImpl implements ShippingService {

    private final ShippingRepository shippingRepository;

    @Override
    @Transactional
    public Optional<ShippingDto> createShipping(Shipping delivery) {
        log.debug("Request to save shipping : {}", delivery);
        return Optional.of(ShippingMapper.INSTANCE.toShippingDto(shippingRepository.save(delivery)));
    }

    @Override
    @Transactional
    public  Optional<ShippingDto> updateShipping(Shipping delivery) {
        log.debug("Request to update shipping : {}", delivery);
        if (delivery.getId() != null) {
            var currentDelivery = shippingRepository.findById(delivery.getId());
            return Optional.of(ShippingMapper.INSTANCE.toShippingDto(currentDelivery.get()));
        }
        return  Optional.of(ShippingMapper.INSTANCE.toShippingDto(delivery));
    }


    @Transactional(readOnly = true)
    @Override
    public Optional<List<ShippingDto>> getAllShippings() {
        log.debug("starting Request to get all shippings");
        return Optional.of(ShippingMapper.INSTANCE.toListShippingDto(shippingRepository.findAll()));
    }


    @Transactional(readOnly = true)
    @Override
    public Optional<ShippingDto> findShippingById(Long id) {
        log.debug("starting Request to get shipping : {}", id);
        return Optional.of(ShippingMapper.INSTANCE.toShippingDto(shippingRepository.findById(id).get()));
    }

    @Transactional
    @Override
    public Optional<Boolean> deleteShipping(Long id) {
        log.debug("Request to delete Delivery : {}", id);
        var entity = shippingRepository.findById(id);
        if (entity != null) {
            shippingRepository.delete(entity.get());
            return Optional.of(Boolean.TRUE);
        }
        return  Optional.of(Boolean.FALSE);
    }
}
