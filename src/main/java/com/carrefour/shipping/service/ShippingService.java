package com.carrefour.shipping.service;

import com.carrefour.shipping.model.Shipping;
import com.carrefour.shipping.service.dto.ShippingDto;

import java.util.List;
import java.util.Optional;


public interface ShippingService {

    Optional<ShippingDto> createShipping(Shipping delivery);

    Optional<ShippingDto> updateShipping(Shipping delivery);
    Optional<List<ShippingDto>> getAllShippings();

    Optional<ShippingDto> findShippingById(Long id);
    Optional<Boolean> deleteShipping(Long id);
}
