package com.carrefour.shipping.controller;

import com.carrefour.shipping.model.Client;
import com.carrefour.shipping.repository.ClientRepository;
import com.carrefour.shipping.service.ClientService;
import com.carrefour.shipping.service.dto.ClientDTO;
import com.carrefour.shipping.util.ShippingRespone;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api/clients")
@AllArgsConstructor
@Slf4j
@Tag(name = "Client api ")
public class ClientController {

    private final ClientService clientService;

    private final ClientRepository clientRepository;


    @Operation(summary = "create a new client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "client created",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @PostMapping("")
    public Callable<ShippingRespone<ClientDTO>> createClient(@RequestBody Client client)  {
        log.debug("sending http request to save Client  : {}", client);
        if (client.getId() == null) {

            return () -> {
                var result = clientService.addNewClient(client);
                return new ShippingRespone<>(HttpStatus.CREATED, "", result.get());
            };
        }
        return null;
    }

    @Operation(summary = "update or create a new client if doesn't exist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "client updated",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @PutMapping("/{id}")
    public Callable<ShippingRespone<ClientDTO>> updateClient(
            @PathVariable(value = "id", required = false) final Long id,
            @RequestBody Client client)  {
        log.debug("sending http to update Client : {}, {}", id, client);
        return () -> {
            var result = clientService.updateClient(client);
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }

    @Operation(summary = "get all clients ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "clients found with success",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @GetMapping(value = "")
    public Callable<ShippingRespone<List<ClientDTO>>>  getAllClientsAsStream() {
        log.debug("sending http request to get all Clients as a stream");
        return () -> {
            var result = clientService.findAllClients();
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }

    @Operation(summary = "search for a specific  clients ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "client found",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @GetMapping("/{id}")
    public Callable<ShippingRespone<ClientDTO>> getClient(@PathVariable("id") Long id) {
        log.debug("sending http request : {}", id);
        return () -> {
            var result = clientService.getClientById(id);
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }

    @Operation(summary = "delete a specific  clients ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "client deleted with success",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @DeleteMapping("/{id}")
    public Callable<ShippingRespone<Boolean>> deleteClient(@PathVariable("id") Long id) {
        log.debug("REST request to delete Client : {}", id);
        return () -> {
            var result = clientService.deleteClient(id);
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }
}
