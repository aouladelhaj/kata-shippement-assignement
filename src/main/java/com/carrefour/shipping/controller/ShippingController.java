package com.carrefour.shipping.controller;

import com.carrefour.shipping.model.Shipping;
import com.carrefour.shipping.service.ShippingService;
import com.carrefour.shipping.service.dto.ShippingDto;
import com.carrefour.shipping.util.ShippingRespone;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;


@RestController
@RequestMapping("/api/shippings")
@RequiredArgsConstructor
@Slf4j
public class ShippingController {

    private final ShippingService shippingService;



    @Operation(summary = "create a new shippement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "shipping created",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ShippingDto.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @PostMapping("")
    public Callable<ShippingRespone<ShippingDto>> createDelivery(@RequestBody Shipping shipping)  {
        log.debug("http request to save Delivery : {}", shipping);

         return () -> {
            var result = shippingService.createShipping(shipping);
            return new ShippingRespone<>(HttpStatus.CREATED, "", result.get());
        };
    }

    @Operation(summary = "update or create a new shippementt")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "shipping created",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ShippingDto.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @PutMapping("/{id}")
    public Callable<ShippingRespone<ShippingDto>> updateShippement(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Shipping shipping
    )  {
        log.debug("http request to update Delivery : {}, {}", id, shipping);

        return () -> {
            var result = shippingService.updateShipping(shipping);
            return new ShippingRespone<>(HttpStatus.CREATED, "", result.get());
        };
    }


    @GetMapping()
    @Operation(summary = "get all shippements ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "shipping created",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ShippingDto.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    public Callable<ShippingRespone<List<ShippingDto>>> getAllShippings() {
        log.debug("REST request to get all Deliveries as a stream");
        return () -> {
            var result = shippingService.getAllShippings();
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }

    @Operation(summary = "search for a specific  shipping ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "shipping created",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ShippingDto.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @GetMapping("/{id}")
    public Callable<ShippingRespone<ShippingDto>> getDelivery(@PathVariable("id") Long id) {
        log.debug("Http REST request to get Delivery : {}", id);
        return () -> {
            var result = shippingService.findShippingById(id);
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }

    @Operation(summary = "delete a specific  shippings ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "shipping created",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ShippingDto.class))}),
            @ApiResponse(responseCode = "400", description = "fonctionnal error", content = @Content),
            @ApiResponse(responseCode = "404", description = "not found", content = @Content)})
    @DeleteMapping("/{id}")
    public Callable<ShippingRespone<Boolean>> deleteDelivery(@PathVariable("id") Long id) {
        log.debug("Http request to delete shipping : {}", id);
        return () -> {
            var result = shippingService.deleteShipping(id);
            return new ShippingRespone<>(HttpStatus.OK, "", result.get());
        };
    }

}
