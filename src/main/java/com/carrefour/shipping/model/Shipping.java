package com.carrefour.shipping.model;

import com.carrefour.shipping.enumeration.ShippingMode;
import com.carrefour.shipping.enumeration.ShippingResult;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Entity Shipping
 */
@Data
@NoArgsConstructor
@Entity
public class Shipping {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Enumerated(EnumType.STRING)
    private ShippingMode mode;

    private LocalTime deliveryTime;
    private LocalTime deliveryEndTime;

    private LocalDate deliveryDate;

    @Enumerated(EnumType.STRING)
    private ShippingResult status;

    private String deliveryAddress;

    @OneToOne(mappedBy = "shipping")
    private Client Client;

}
