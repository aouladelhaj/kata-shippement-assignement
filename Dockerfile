FROM openjdk:21-ea-11
EXPOSE 8080
WORKDIR /
COPY . .
RUN chmod +x mvnw
RUN mvn clean package spring-boot:repackage -DskipTests=true
ENTRYPOINT ["java","-jar","/target/shipping-kata.jar"]
